<?php
try {
  $bdd = new PDO('mysql:host=localhost;dbname=tests;charset=utf8', 'root', '');
}
catch (Exception $e) {
  die('Erreur : ' . $e->getMessage());
}
$req= $bdd->prepare('SELECT id,pass FROM membres where Pseudo=:pseudo');
$req->execute(array(
  'pseudo'=>$_POST['pseudo']
));
$resultat = $req->fetch();
$verifpass= password_verify($_POST['pass'],$resultat['pass']);
if (!$resultat) echo "Mauvais identifiant ou mdp";
else {
  if($verifpass)
  {
    echo "Vous êtes connectés";
    session_start();
    $_SESSION['id']=$resultat['id'];
    $_SESSION['pseudo']=$_POST['pseudo'];
    header('Location: Page_connecte.php');
  }
  else echo "Mauvais mdp";
}
 ?>
