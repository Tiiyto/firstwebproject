<?php
  try {
    $bdd = new PDO('mysql:host=localhost;dbname=tests;charset=utf8', 'root', '');
  }
  catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
  }
  $pass_hache= password_hash($_POST['password'],PASSWORD_DEFAULT);
  $rep=$bdd->prepare('INSERT INTO membres(pseudo,pass,email,date_inscription) VALUES (:pseudo,:pass,:email,CURDATE())');
  $rep->execute(array(
  'pseudo'=>$_POST['pseudo'],
  'pass' => $pass_hache,
  'email' =>$_POST['email']
  ));
  header('Location: Page_connexion.php');
 ?>
